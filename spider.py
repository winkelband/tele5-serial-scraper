'''
    Download list of current serial episodes on Tele5.de
    [requires Scrapy]

    Usage:
        scrapy runspider --nolog spider.py
'''


import re
import scrapy
import pandas as pd
import pandas.errors as pd_err


class EpisodeSpider(scrapy.Spider):
    name = 'episode_spider'

    allowed_domains = ['tele5.de']

    def __init__(self):
        # Create lists for Pandas dataframe.
        self.video_uris = []
        self.video_serials = []
        self.video_subserials = []
        self.video_season_nos = []
        self.video_episode_nos = []
        self.video_titles = []

    def start_requests(self):
        # Define serials to grab video episodes for.
        urls = [
            'https://www.tele5.de/star-trek/das-nachste-jahrhundert/',
            'https://www.tele5.de/star-trek/raumschiff-voyager/',
            'https://www.tele5.de/star-trek/enterprise/',
            'https://www.tele5.de/starhunter/',
            'https://www.tele5.de/timeless/',
            'https://www.tele5.de/relic-hunter/',
            'https://www.tele5.de/the-quest/',
            'https://www.tele5.de/flashpoint/',
            'https://www.tele5.de/sea-patrol/',
        ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        '''Get links to individual videos'''
        video_urls = response.xpath('//*[@class="video-list"]/div\
                                   [starts-with(@class, "list--video  ")]\
                                   /a/@href'
                                   ).extract()

        for video_url in video_urls:
            print(video_url)
            yield scrapy.Request(url=response.urljoin(video_url),
                                 callback=self.parse_video)

    def parse_video(self, response):
        '''Parse information of individual videos'''

        # Get url path for active video.
        episode_link = response.xpath(
            '//*[@class="video-list"]/\
            div[@class="list--video  active-video"]/a/@href').extract_first()
        video_uri = response.urljoin(episode_link)
        print('Full URL ' + video_uri)

        # Get title, season no. and episode no.
        video_meta = response.xpath('//*[@class="mediathek-video-player"]/\
                                    div[@id]/@data-title'
                                   ).extract_first().split()[-1]
        video_meta = re.search('s(\d+)e(\d+)', video_meta)
        video_serial = video_uri.split('/')[3]
        none_subserials = ['ganze-folge',]
        if video_uri.split('/')[4] in none_subserials:
            video_subserial = ''
        else:
            video_subserial = video_uri.split('/')[4]
        video_season_no = video_meta.group(1).lstrip('0') 
        video_episode_no = video_meta.group(2).lstrip('0') 
        video_title = response.xpath('//*[@class="video-title"]/text()'
                                    ).extract_first()
        print('Serial ' + video_serial)
        print('Subserial ' + video_subserial)
        print('Season No. ' + video_season_no)
        print('Episode No. ' + video_episode_no)

        # Append information to existing lists.
        self.video_uris.append(video_uri)
        self.video_serials.append(video_serial)
        self.video_subserials.append(video_subserial)
        self.video_season_nos.append(video_season_no)
        self.video_episode_nos.append(video_episode_no)
        self.video_titles.append(video_title)
        
        # Create pandas Dataframe and save lists to associated columns.
        output_filename = 'tele-5-seriallist.csv'
        df = pd.DataFrame(columns=['video_serial',
                                   'video_subserial',
                                   'video_season_no',
                                   'video_episode_no',
                                   'video_title',
                                   'video_uri'])
        df['video_uri'] = self.video_uris
        df['video_serial'] = self.video_serials
        df['video_subserial'] = self.video_subserials
        df['video_season_no'] = self.video_season_nos
        df['video_episode_no'] = self.video_episode_nos
        df['video_title'] = self.video_titles

        # Output Dataframe as csv sheet.
        df.to_csv(output_filename, index=False)
